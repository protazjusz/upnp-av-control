package pl.umk.mat.protazy.upnpavcontrol;

import java.util.ArrayList;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.ModelUtil;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.RemoteService;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.support.avtransport.callback.GetPositionInfo;
import org.teleal.cling.support.avtransport.callback.Pause;
import org.teleal.cling.support.avtransport.callback.Play;
import org.teleal.cling.support.avtransport.callback.Seek;
import org.teleal.cling.support.avtransport.callback.SetAVTransportURI;
import org.teleal.cling.support.avtransport.callback.Stop;
import org.teleal.cling.support.contentdirectory.DIDLParser;
import org.teleal.cling.support.contentdirectory.callback.Browse;
import org.teleal.cling.support.model.BrowseFlag;
import org.teleal.cling.support.model.DIDLContent;
import org.teleal.cling.support.model.DIDLObject;
import org.teleal.cling.support.model.PositionInfo;
import org.teleal.cling.support.model.container.Container;
import org.teleal.cling.support.model.item.Item;
import org.teleal.cling.support.renderingcontrol.callback.GetMute;
import org.teleal.cling.support.renderingcontrol.callback.GetVolume;
import org.teleal.cling.support.renderingcontrol.callback.SetMute;
import org.teleal.cling.support.renderingcontrol.callback.SetVolume;

import android.app.Activity;

public class ActionHelper {

	public String ROOT_DIR = "0";

	public ActionHelper() {
	}

	public ActionHelper(Activity activity) {
		currentActivity = activity;
	}

	private Activity currentActivity;

	private RemoteService contentDirectoryService;

	public RemoteService getContentDirectoryService() {
		return contentDirectoryService;
	}

	public void setContentDirectoryService(RemoteService service) {
		contentDirectoryService = service;
	}

	private RemoteService renderingControlService;

	public RemoteService getRenderingControlService() {
		return renderingControlService;
	}

	public void setRenderingControlService(RemoteService service) {
		renderingControlService = service;
	}

	private RemoteService avTransportService;

	public RemoteService getAVTransportService() {
		return avTransportService;
	}

	public void setAVTransportService(RemoteService service) {
		avTransportService = service;
	}

	private AndroidUpnpService getUpnpService() {
		return ((MainActivity) currentActivity).getUpnpService();
	}

	private ControlPoint getControlPoint() {
		return getUpnpService().getControlPoint();
	}

	public FileArrayAdapter getFileArrayAdapter() {
		return ((MainActivity) currentActivity).getMediaServerFragment()
				.getFileArrayAdapter();
	}

	private DeviceItem getCurrentMediaServer() {
		return ((MainActivity) currentActivity).getDeviceArrayAdapter()
				.getCurrentMediaServer();
	}

	private DeviceItem getCurrentMediaRenderer() {
		return ((MainActivity) currentActivity).getDeviceArrayAdapter()
				.getCurrentMediaRenderer();
	}

	private void setCurrentMediaInfo(Item item) {
		((MainActivity) currentActivity).getMediaRendererFragment()
				.setCurrentItemInfo(item);
	}

	private Boolean isUriSet;

	public ArrayList<DIDLObject> browseAction(final String directory) {

		final ArrayList<DIDLObject> objectArray = new ArrayList<DIDLObject>();

		if (getCurrentMediaServer() != null
				&& getContentDirectoryService() != null) {

			ActionCallback browseAction = new Browse(
					getContentDirectoryService(), directory,
					BrowseFlag.DIRECT_CHILDREN) {
				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation invocation,
						UpnpResponse operation, String defaultMsg) {
				}

				@SuppressWarnings("rawtypes")
				@Override
				public void received(ActionInvocation arg0, DIDLContent arg1) {

					getFileArrayAdapter().clear();
					if (!directory.equals(ROOT_DIR)) {
						Container rootContainer = new Container();
						rootContainer.setTitle(currentActivity
								.getString(R.string.server_root_dir));
						rootContainer.setId(ROOT_DIR);

						objectArray.add(rootContainer);
					}
					for (final Container container : arg1.getContainers()) {
						objectArray.add(container);
					}
					for (final Item item : arg1.getItems()) {
						objectArray.add(item);
					}
				}

				@Override
				public void updateStatus(Status arg0) {
				}
			};
			try {
				browseAction.setControlPoint(getControlPoint());
				browseAction.run();
			} catch (Exception e) {
			}
		} else {
			getFileArrayAdapter().clear();
		}

		return objectArray;
	}

	public Boolean setAVTransportUriAction(DIDLObject item) {

		isUriSet = true;

		if (getCurrentMediaRenderer() != null
				&& getAVTransportService() != null) {

			String uri = item.getFirstResource().getValue();

			DIDLParser parser = new DIDLParser();
			DIDLContent didl = new DIDLContent();
			String metadata = "NULL";
			didl.addItem((Item) item);
			try {
				metadata = parser.generate(didl, true);
			} catch (Exception e) {
			}

			ActionCallback setAVTransportURIAction = new SetAVTransportURI(
					getAVTransportService(), uri, metadata) {
				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation invocation,
						UpnpResponse operation, String defaultMsg) {

					isUriSet = false;
				}
			};

			try {
				setAVTransportURIAction.setControlPoint(getControlPoint());
				setAVTransportURIAction.run();

			} catch (Exception e) {
			}
		}

		return isUriSet;
	}

	public void getPositionInfoAction() {
		if (getCurrentMediaRenderer() != null
				&& getAVTransportService() != null) {

			GetPositionInfo getMediaInfoAction = new GetPositionInfo(
					getAVTransportService()) {
				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation invocation,
						UpnpResponse operation, String defaultMsg) {
				}

				@SuppressWarnings("rawtypes")
				@Override
				public void received(ActionInvocation arg0, PositionInfo arg1) {
					if (arg1 != null) {
						DIDLParser parser = new DIDLParser();
						DIDLContent content = new DIDLContent();
						try {
							content = parser.parse(arg1.getTrackMetaData());

						} catch (Exception e) {
						}
						setCurrentMediaInfo(content.getItems().get(0));
					}
				}
			};

			try {
				getMediaInfoAction.setControlPoint(getControlPoint());
				getMediaInfoAction.run();

			} catch (Exception e) {
			}
		}
	}

	public void playAction() {
		if (getCurrentMediaRenderer() != null
				&& getAVTransportService() != null) {

			ActionCallback playAction = new Play(getAVTransportService()) {
				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation invocation,
						UpnpResponse operation, String defaultMsg) {
				}
			};

			try {
				playAction.setControlPoint(getControlPoint());
				playAction.run();

			} catch (Exception e) {
			}

		}

	}

	public void stopAction() {
		if (getCurrentMediaRenderer() != null
				&& getAVTransportService() != null) {

			ActionCallback stopAction = new Stop(getAVTransportService()) {
				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation invocation,
						UpnpResponse operation, String defaultMsg) {
				}
			};

			try {
				stopAction.setControlPoint(getControlPoint());
				stopAction.run();
			} catch (Exception e) {
			}
		} 
	}

	public void pauseAction() {
		if (getCurrentMediaRenderer() != null
				&& getAVTransportService() != null) {

			ActionCallback pauseAction = new Pause(getAVTransportService()) {
				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation invocation,
						UpnpResponse operation, String defaultMsg) {
				}
			};

			try {
				pauseAction.setControlPoint(getControlPoint());
				pauseAction.run();
			} catch (Exception e) {
			}

		}
	}

	@SuppressWarnings("rawtypes")
	private ActionCallback seek(Service avtService, String targetTime) {
		ActionCallback seekAction = new Seek(avtService, targetTime) {
			@Override
			public void failure(ActionInvocation invocation,
					UpnpResponse operation, String defaultMsg) {
			}
		};
		return seekAction;
	}

	public void seekAction(final long delta) {
		if (getCurrentMediaRenderer() != null
				&& getAVTransportService() != null) {

			ActionCallback action = new GetPositionInfo(getAVTransportService()) {

				@SuppressWarnings("rawtypes")
				@Override
				public void received(ActionInvocation arg0, PositionInfo arg1) {

					long seconds = arg1.getTrackElapsedSeconds();
					long targetSeconds = seconds + delta;
					String target = ModelUtil.toTimeString(targetSeconds);

					ActionCallback seekAction = seek(getAVTransportService(),
							target);
					try {
						seekAction.setControlPoint(getControlPoint());
						seekAction.run();
					} catch (Exception e) {
					}

				}

				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation arg0, UpnpResponse arg1,
						String arg2) {
				}
			};

			try {
				action.setControlPoint(getControlPoint());
				action.run();

			} catch (Exception e) {
			}
		} 

	}

	@SuppressWarnings("rawtypes")
	private ActionCallback setMute(Service rcService, Boolean value) {
		ActionCallback setMuteAction = new SetMute(rcService, value) {
			@Override
			public void failure(ActionInvocation invocation,
					UpnpResponse operation, String defaultMsg) {
			}
		};
		return setMuteAction;
	}

	public void setMuteAction() {

		if (getCurrentMediaRenderer() != null
				&& getRenderingControlService() != null) {

			ActionCallback action = new GetMute(getRenderingControlService()) {

				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation arg0, UpnpResponse arg1,
						String arg2) {
				}

				@SuppressWarnings("rawtypes")
				@Override
				public void received(ActionInvocation arg0, boolean arg1) {

					ActionCallback setMuteAction = setMute(
							getRenderingControlService(), !arg1);
					try {
						setMuteAction.setControlPoint(controlPoint);
						setMuteAction.run();
					} catch (Exception e) {
					}
				}
			};

			try {
				action.setControlPoint(getControlPoint());
				action.run();
			} catch (Exception e) {
			}
		} 
	}

	@SuppressWarnings("rawtypes")
	private ActionCallback setVolume(Service rcService, int targetVolume) {
		ActionCallback setVolumeAction = new SetVolume(rcService, targetVolume) {
			@Override
			public void failure(ActionInvocation invocation,
					UpnpResponse operation, String defaultMsg) {
			}
		};
		return setVolumeAction;
	}

	public void setVolumeAction(final int delta) {
		if (getCurrentMediaRenderer() != null
				&& getRenderingControlService() != null) {

			ActionCallback action = new GetVolume(getRenderingControlService()) {

				@SuppressWarnings("rawtypes")
				@Override
				public void failure(ActionInvocation arg0, UpnpResponse arg1,
						String arg2) {
				}

				@SuppressWarnings("rawtypes")
				@Override
				public void received(ActionInvocation arg0, int arg1) {

					int targetVol = arg1 + delta;

					ActionCallback setVolumeAction = setVolume(
							getRenderingControlService(), targetVol);

					try {
						setVolumeAction.setControlPoint(controlPoint);
						setVolumeAction.run();
					} catch (Exception e) {
					}
				}
			};

			try {
				action.setControlPoint(getControlPoint());
				action.run();
			} catch (Exception e) {
			}
		} 
	}

}
