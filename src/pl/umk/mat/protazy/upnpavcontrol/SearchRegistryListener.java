package pl.umk.mat.protazy.upnpavcontrol;

import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;

import android.app.Activity;

class SearchRegistryListener extends DefaultRegistryListener {

	private Activity currentActivity;
	private DeviceArrayAdapter arrayAdapter;

	public SearchRegistryListener() {
	}

	public SearchRegistryListener(Activity activity, DeviceArrayAdapter adapter) {
		currentActivity = activity;
		arrayAdapter = adapter;
	}

	@Override
	public void remoteDeviceDiscoveryStarted(Registry registry,
			RemoteDevice device) {
		deviceAdded(device);
	}

	@Override
	public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
		deviceAdded(device);
	}

	@Override
	public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
		deviceRemoved(device);
	}

	@Override
	public void localDeviceAdded(Registry registry, LocalDevice device) {
		deviceAdded(device);
	}

	@Override
	public void localDeviceRemoved(Registry registry, LocalDevice device) {
		deviceRemoved(device);
	}

	@SuppressWarnings("rawtypes")
	public void deviceAdded(final Device device) {
		currentActivity.runOnUiThread(new Runnable() {
			public void run() {
				DeviceItem d = new DeviceItem(currentActivity, device);
				int position = arrayAdapter.getPosition(d);
				if (position >= 0) {
					arrayAdapter.remove(d);
					arrayAdapter.insert(d, position);
				} else
					arrayAdapter.add(d);
			}
		});
	}

	@SuppressWarnings("rawtypes")
	public void deviceRemoved(final Device device) {
		currentActivity.runOnUiThread(new Runnable() {
			public void run() {
				arrayAdapter.remove(new DeviceItem(currentActivity, device));
			}
		});
	}

	@Override
	public void remoteDeviceDiscoveryFailed(Registry registry,
			final RemoteDevice device, Exception ex) {
		currentActivity.runOnUiThread(new Runnable() {
			public void run() {
				deviceRemoved(device);
			}
		});
	}
}