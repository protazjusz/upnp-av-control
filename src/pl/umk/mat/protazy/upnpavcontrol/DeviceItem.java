package pl.umk.mat.protazy.upnpavcontrol;

import java.net.URI;
import java.net.URL;

import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.DeviceDetails;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.RemoteService;
import org.teleal.cling.model.types.UDAServiceType;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;

class DeviceItem {

	@SuppressWarnings("rawtypes")
	public DeviceItem(Context context, Device device) {
		this.context = context;
		this.device = device;
	}

	@SuppressWarnings("rawtypes")
	Device device;
	Context context;
	Bitmap icon = null;

	@SuppressWarnings("rawtypes")
	public Device getDevice() {
		return device;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		DeviceItem that = (DeviceItem) o;
		return device.equals(that.device);
	}

	@Override
	public int hashCode() {
		return device.hashCode();
	}

	@Override
	public String toString() {
		if (device.isFullyHydrated())
			return device.getDetails().getFriendlyName();
		else
			return context.getString(R.string.dev_display_processing)
					+ device.getDetails().getFriendlyName();
	}

	public Boolean isFullyHydrated() {
		return device.isFullyHydrated();
	}

	public RemoteService getService(String serviceType) {
		RemoteDevice rd = (RemoteDevice) this.getDevice();
		RemoteService service = rd.findService(new UDAServiceType(serviceType));
		return service;
	}

	public String getType() {
		return device.getType().getType();
	}

	public Boolean isMediaServer() {
		return getType().equals("MediaServer");
	}

	public Boolean isMediaRenderer() {
		return getType().equals("MediaRenderer");
	}

	public Bitmap getIcon(Bitmap alternativeIcon) {
		if (icon == null) {

			if (device.hasIcons()) {
				URI uri;
				RemoteDevice rdevice = (RemoteDevice) device;

				ThreadPolicy tp = ThreadPolicy.LAX;
				StrictMode.setThreadPolicy(tp);

				try {
					uri = device.getIcons()[0].getUri();
					icon = BitmapFactory.decodeStream(rdevice.normalizeURI(uri)
							.openConnection().getInputStream());
				} catch (Exception e) {
					icon = alternativeIcon;
				}
			} else {
				icon = alternativeIcon;
			}
		}
		return icon;
	}

	public String getDisplayString() {
		return device.getDisplayString();
	}

	public String getAllInfo() {
		StringBuilder info = new StringBuilder();
		DeviceDetails deviceDetails = device.getDetails();

		info.append(context.getString(R.string.dev_display_type)
				+ device.getType().getType() + ":"
				+ device.getType().getVersion() + "\n");

		info.append(context.getString(R.string.dev_display_manufacturer)
				+ deviceDetails.getManufacturerDetails().getManufacturer()
				+ "\n");
		info.append(context.getString(R.string.dev_display_model)
				+ deviceDetails.getModelDetails().getModelName() + "\n");
		info.append(context.getString(R.string.dev_display_model_desc)
				+ deviceDetails.getModelDetails().getModelDescription() + "\n");

		RemoteDevice rd = (RemoteDevice) device;
		URL descriptorURL = rd.getIdentity().getDescriptorURL();
		info.append(context.getString(R.string.dev_display_host)
				+ descriptorURL.getHost() + "\n");

		if (deviceDetails.getPresentationURI() != null)
			info.append(context.getString(R.string.dev_display_gui)
					+ deviceDetails.getPresentationURI().toString() + "\n");

		info.append("\n");
		for (RemoteService rs : rd.getServices())
			info.append(context.getString(R.string.dev_display_service)
					+ rs.getServiceType().getType() + ":"
					+ rs.getServiceType().getVersion() + "\n");

		if (device.hasEmbeddedDevices())
			info.append(context.getString(R.string.dev_display_embedded)
					+ (device.getEmbeddedDevices().length) + "\n");

		info.append(context.getString(R.string.dev_display_infoxml)
				+ descriptorURL.toString());

		return info.toString();
	}
}