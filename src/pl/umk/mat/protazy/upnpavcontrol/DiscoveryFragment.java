package pl.umk.mat.protazy.upnpavcontrol;

import java.util.ArrayList;

import org.teleal.cling.model.meta.RemoteService;
import org.teleal.cling.support.model.DIDLObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DiscoveryFragment extends Fragment {

	private Context context;

	private DeviceArrayAdapter getDeviceArrayAdapter() {
		return ((MainActivity) getActivity()).getDeviceArrayAdapter();
	}

	private ActionHelper getActionHelper() {
		return ((MainActivity) getActivity()).getActionHelper();
	}

	private void setupMediaServer(DeviceItem device) {
		RemoteService service = device.getService("ContentDirectory");
		getDeviceArrayAdapter().setCurrentMediaServer(device);
		getActionHelper().setContentDirectoryService(service);
	}

	private void setupMediaRenderer(DeviceItem device) {
		RemoteService rcService = device.getService("RenderingControl");
		getDeviceArrayAdapter().setCurrentMediaRenderer(device);
		getActionHelper().setRenderingControlService(rcService);

		RemoteService avtService = device.getService("AVTransport");
		getActionHelper().setAVTransportService(avtService);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_discovery,
				container, false);

		context = getActivity();

		ListView deviceListView = (ListView) rootView
				.findViewById(R.id.discoveryListView);
		deviceListView.setAdapter(getDeviceArrayAdapter());

		deviceListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				DeviceItem deviceListItem = getDeviceArrayAdapter().getItem(
						position);

				AlertDialog.Builder aboutDeviceAlert = new AlertDialog.Builder(
						getActivity());
				aboutDeviceAlert.setTitle(deviceListItem.getDisplayString());
				aboutDeviceAlert.setMessage(deviceListItem.getAllInfo());
				final TextView input = new TextView(getActivity());
				aboutDeviceAlert.setView(input);
				aboutDeviceAlert.show();
			}
		});

		deviceListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {

						DeviceItem item = getDeviceArrayAdapter().getItem(
								position);

						if (!item.isFullyHydrated()) {
							Toast.makeText(
									context,
									context.getString(R.string.discovery_processing),
									Toast.LENGTH_SHORT).show();
						} else {

							if (item.isMediaServer()) {
								setupMediaServer(item);

								ArrayList<DIDLObject> objectList = getActionHelper()
										.browseAction(
												getActionHelper().ROOT_DIR);
								getActionHelper().getFileArrayAdapter().addAll(
										objectList);

								Toast.makeText(
										context,
										context.getString(R.string.discovery_server),
										Toast.LENGTH_SHORT).show();

							} else if (item.isMediaRenderer()) {
								setupMediaRenderer(item);

								Toast.makeText(
										context,
										context.getString(R.string.discovery_renderer),
										Toast.LENGTH_SHORT).show();

							} else {
								Toast.makeText(
										context,
										context.getString(R.string.discovery_other),
										Toast.LENGTH_SHORT).show();
							}
						}
						return true;
					}
				});

		return rootView;
	}
}
