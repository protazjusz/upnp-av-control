package pl.umk.mat.protazy.upnpavcontrol;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DeviceArrayAdapter extends ArrayAdapter<DeviceItem> {

	public DeviceArrayAdapter(final Context context, final int resource) {
		super(context, resource);

		inflater = LayoutInflater.from(context);
	}

	private final LayoutInflater inflater;

	private ViewHolder holder;

	private DeviceItem currentMediaServer;

	public DeviceItem getCurrentMediaServer() {
		return currentMediaServer;
	}

	public void setCurrentMediaServer(DeviceItem device) {
		currentMediaServer = device;
	}

	private DeviceItem currentMediaRenderer;

	public DeviceItem getCurrentMediaRenderer() {
		return currentMediaRenderer;
	}

	public void setCurrentMediaRenderer(DeviceItem device) {
		currentMediaRenderer = device;
	}

	@Override
	public View getView(final int position, final View convertView,
			final ViewGroup parent) {

		View itemView = convertView;
		holder = null;
		final DeviceItem item = getItem(position);

		if (null == itemView) {
			itemView = this.inflater.inflate(R.layout.discovery_list_item,
					parent, false);

			holder = new ViewHolder();

			holder.deviceName = (TextView) itemView
					.findViewById(R.id.deviceName);
			holder.deviceType = (TextView) itemView
					.findViewById(R.id.deviceDescription);
			holder.deviceIcon = (ImageView) itemView
					.findViewById(R.id.deviceIcon);

			itemView.setTag(holder);
		} else {
			holder = (ViewHolder) itemView.getTag();
		}

		holder.deviceName.setText(item.toString());

		holder.deviceType.setText(item.getType());

		Bitmap icon = item.getIcon(BitmapFactory.decodeResource(getContext()
				.getResources(), R.drawable.unknown));
		holder.deviceIcon.setImageBitmap(icon);

		return itemView;
	}

	@Override
	public void clear() {
		super.clear();
	}

	@Override
	public void remove(DeviceItem device) {
		Context context = getContext();
		if (device.equals(currentMediaServer)) {
			currentMediaServer = null;
			Toast.makeText(context,
					context.getString(R.string.dev_display_server_gone),
					Toast.LENGTH_LONG).show();
		} else if (device.equals(currentMediaRenderer)) {

			currentMediaRenderer = null;
			Toast.makeText(context,
					context.getString(R.string.dev_display_renderer_gone),
					Toast.LENGTH_LONG).show();
		}
		super.remove(device);
	}

	private static class ViewHolder {

		ViewHolder() {
		}

		private TextView deviceName;
		private TextView deviceType;
		private ImageView deviceIcon;
	}
}