package pl.umk.mat.protazy.upnpavcontrol;

import java.util.Locale;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.android.AndroidUpnpServiceImpl;
import org.teleal.cling.model.meta.Device;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.support.v13.app.FragmentPagerAdapter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class MainActivity extends Activity implements ActionBar.TabListener {

	/**
	 * Licencja LGPL 3.0, która dotyczy ikon: 'multimedia-volume-control',
	 * 'unknown', 'inode-directory', 'image-x-generic', 'audio-x-generic',
	 * 'video-x-generic', pochodzących z pakietu Oxygen Icons:
	 * http://www.oxygen-icons.org
	 * 
	 * oraz biblioteki Cling:
	 * http://4thline.org/projects/cling
	 * 
	 * ---
	 * 
	 * GNU LESSER GENERAL PUBLIC LICENSE Version 3, 29 June 2007
	 * 
	 * Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
	 * Everyone is permitted to copy and distribute verbatim copies of this
	 * license document, but changing it is not allowed.
	 * 
	 * 
	 * This version of the GNU Lesser General Public License incorporates the
	 * terms and conditions of version 3 of the GNU General Public License,
	 * supplemented by the additional permissions listed below.
	 * 
	 * 0. Additional Definitions.
	 * 
	 * As used herein, "this License" refers to version 3 of the GNU Lesser
	 * General Public License, and the "GNU GPL" refers to version 3 of the GNU
	 * General Public License.
	 * 
	 * "The Library" refers to a covered work governed by this License, other
	 * than an Application or a Combined Work as defined below.
	 * 
	 * An "Application" is any work that makes use of an interface provided by
	 * the Library, but which is not otherwise based on the Library. Defining a
	 * subclass of a class defined by the Library is deemed a mode of using an
	 * interface provided by the Library.
	 * 
	 * A "Combined Work" is a work produced by combining or linking an
	 * Application with the Library. The particular version of the Library with
	 * which the Combined Work was made is also called the "Linked Version".
	 * 
	 * The "Minimal Corresponding Source" for a Combined Work means the
	 * Corresponding Source for the Combined Work, excluding any source code for
	 * portions of the Combined Work that, considered in isolation, are based on
	 * the Application, and not on the Linked Version.
	 * 
	 * The "Corresponding Application Code" for a Combined Work means the object
	 * code and/or source code for the Application, including any data and
	 * utility programs needed for reproducing the Combined Work from the
	 * Application, but excluding the System Libraries of the Combined Work.
	 * 
	 * 1. Exception to Section 3 of the GNU GPL.
	 * 
	 * You may convey a covered work under sections 3 and 4 of this License
	 * without being bound by section 3 of the GNU GPL.
	 * 
	 * 2. Conveying Modified Versions.
	 * 
	 * If you modify a copy of the Library, and, in your modifications, a
	 * facility refers to a function or data to be supplied by an Application
	 * that uses the facility (other than as an argument passed when the
	 * facility is invoked), then you may convey a copy of the modified version:
	 * 
	 * a) under this License, provided that you make a good faith effort to
	 * ensure that, in the event an Application does not supply the function or
	 * data, the facility still operates, and performs whatever part of its
	 * purpose remains meaningful, or
	 * 
	 * b) under the GNU GPL, with none of the additional permissions of this
	 * License applicable to that copy.
	 * 
	 * 3. Object Code Incorporating Material from Library Header Files.
	 * 
	 * The object code form of an Application may incorporate material from a
	 * header file that is part of the Library. You may convey such object code
	 * under terms of your choice, provided that, if the incorporated material
	 * is not limited to numerical parameters, data structure layouts and
	 * accessors, or small macros, inline functions and templates (ten or fewer
	 * lines in length), you do both of the following:
	 * 
	 * a) Give prominent notice with each copy of the object code that the
	 * Library is used in it and that the Library and its use are covered by
	 * this License.
	 * 
	 * b) Accompany the object code with a copy of the GNU GPL and this license
	 * document.
	 * 
	 * 4. Combined Works.
	 * 
	 * You may convey a Combined Work under terms of your choice that, taken
	 * together, effectively do not restrict modification of the portions of the
	 * Library contained in the Combined Work and reverse engineering for
	 * debugging such modifications, if you also do each of the following:
	 * 
	 * a) Give prominent notice with each copy of the Combined Work that the
	 * Library is used in it and that the Library and its use are covered by
	 * this License.
	 * 
	 * b) Accompany the Combined Work with a copy of the GNU GPL and this
	 * license document.
	 * 
	 * c) For a Combined Work that displays copyright notices during execution,
	 * include the copyright notice for the Library among these notices, as well
	 * as a reference directing the user to the copies of the GNU GPL and this
	 * license document.
	 * 
	 * d) Do one of the following:
	 * 
	 * 0) Convey the Minimal Corresponding Source under the terms of this
	 * License, and the Corresponding Application Code in a form suitable for,
	 * and under terms that permit, the user to recombine or relink the
	 * Application with a modified version of the Linked Version to produce a
	 * modified Combined Work, in the manner specified by section 6 of the GNU
	 * GPL for conveying Corresponding Source.
	 * 
	 * 1) Use a suitable shared library mechanism for linking with the Library.
	 * A suitable mechanism is one that (a) uses at run time a copy of the
	 * Library already present on the user's computer system, and (b) will
	 * operate properly with a modified version of the Library that is
	 * interface-compatible with the Linked Version.
	 * 
	 * e) Provide Installation Information, but only if you would otherwise be
	 * required to provide such information under section 6 of the GNU GPL, and
	 * only to the extent that such information is necessary to install and
	 * execute a modified version of the Combined Work produced by recombining
	 * or relinking the Application with a modified version of the Linked
	 * Version. (If you use option 4d0, the Installation Information must
	 * accompany the Minimal Corresponding Source and Corresponding Application
	 * Code. If you use option 4d1, you must provide the Installation
	 * Information in the manner specified by section 6 of the GNU GPL for
	 * conveying Corresponding Source.)
	 * 
	 * 5. Combined Libraries.
	 * 
	 * You may place library facilities that are a work based on the Library
	 * side by side in a single library together with other library facilities
	 * that are not Applications and are not covered by this License, and convey
	 * such a combined library under terms of your choice, if you do both of the
	 * following:
	 * 
	 * a) Accompany the combined library with a copy of the same work based on
	 * the Library, uncombined with any other library facilities, conveyed under
	 * the terms of this License.
	 * 
	 * b) Give prominent notice with the combined library that part of it is a
	 * work based on the Library, and explaining where to find the accompanying
	 * uncombined form of the same work.
	 * 
	 * 6. Revised Versions of the GNU Lesser General Public License.
	 * 
	 * The Free Software Foundation may publish revised and/or new versions of
	 * the GNU Lesser General Public License from time to time. Such new
	 * versions will be similar in spirit to the present version, but may differ
	 * in detail to address new problems or concerns.
	 * 
	 * Each version is given a distinguishing version number. If the Library as
	 * you received it specifies that a certain numbered version of the GNU
	 * Lesser General Public License "or any later version" applies to it, you
	 * have the option of following the terms and conditions either of that
	 * published version or of any later version published by the Free Software
	 * Foundation. If the Library as you received it does not specify a version
	 * number of the GNU Lesser General Public License, you may choose any
	 * version of the GNU Lesser General Public License ever published by the
	 * Free Software Foundation.
	 * 
	 * If the Library as you received it specifies that a proxy can decide
	 * whether future versions of the GNU Lesser General Public License shall
	 * apply, that proxy's public statement of acceptance of any version is
	 * permanent authorization for you to choose that version for the Library.
	 * 
	 * ---
	 * 
	 * W oprogramowaniu wykorzystano także ikony z pakietu 
	 * Android Action Bar Icon Pack:
	 * https://developer.android.com/design/downloads/index.html
	 * dostępnego na licencji CC BY 2.5:
	 * http://creativecommons.org/licenses/by/2.5/
	 */

	SectionsPagerAdapter mSectionsPagerAdapter;

	ViewPager mViewPager;

	private DiscoveryFragment discoveryFragment;

	public DiscoveryFragment getDiscoveryFragment() {
		return discoveryFragment;
	}

	private MediaServerFragment mediaServerFragment;

	public MediaServerFragment getMediaServerFragment() {
		return mediaServerFragment;
	}

	private MediaRendererFragment mediaRendererFragment;

	public MediaRendererFragment getMediaRendererFragment() {
		return mediaRendererFragment;
	}

	private AndroidUpnpService upnpService;

	public AndroidUpnpService getUpnpService() {
		return upnpService;
	}

	private DeviceArrayAdapter deviceArrayAdapter;

	public DeviceArrayAdapter getDeviceArrayAdapter() {
		return deviceArrayAdapter;
	}

	private ActionHelper actionHelper;

	public ActionHelper getActionHelper() {
		return actionHelper;
	}

	private SearchRegistryListener registryListener;

	private Context context;

	private boolean isWiFiNetworkAvailable() {
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		return mWifi.isConnected();
	}

	private ServiceConnection upnpServiceConnection = new ServiceConnection() {

		@SuppressWarnings("rawtypes")
		public void onServiceConnected(ComponentName className, IBinder service) {
			upnpService = (AndroidUpnpService) service;

			if (!deviceArrayAdapter.isEmpty())
				deviceArrayAdapter.clear();

			for (Device device : upnpService.getRegistry().getDevices()) {
				registryListener.deviceAdded(device);
			}

			upnpService.getRegistry().addListener(registryListener);

			upnpService.getControlPoint().search();
		}

		public void onServiceDisconnected(ComponentName className) {
			upnpService = null;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOffscreenPageLimit(3);

		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}

		context = getApplicationContext();

		deviceArrayAdapter = new DeviceArrayAdapter(this,
				R.layout.discovery_list_item);
		registryListener = new SearchRegistryListener(this, deviceArrayAdapter);

		context.bindService(new Intent(this, AndroidUpnpServiceImpl.class),
				upnpServiceConnection, Context.BIND_AUTO_CREATE);

		discoveryFragment = new DiscoveryFragment();
		mediaServerFragment = new MediaServerFragment();
		mediaRendererFragment = new MediaRendererFragment();

		actionHelper = new ActionHelper(this);

		if (!isWiFiNetworkAvailable()) {
			Toast.makeText(context,
					context.getString(R.string.activity_no_connection),
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.menu_info) {
			StringBuilder menu = new StringBuilder();

			DeviceItem mServer = deviceArrayAdapter.getCurrentMediaServer();
			if (mServer == null)
				menu.append(getString(R.string.menu_info_server_none));
			else
				menu.append(getString(R.string.menu_info_server_ok) + mServer
						+ "\n");

			DeviceItem mRenderer = deviceArrayAdapter.getCurrentMediaRenderer();
			if (mRenderer == null)
				menu.append(getString(R.string.menu_info_renderer_none));
			else
				menu.append(getString(R.string.menu_info_renderer_ok)
						+ mRenderer);

			Toast.makeText(getApplicationContext(), menu, Toast.LENGTH_LONG)
					.show();

			return true;
		} else if (id == R.id.menu_refresh) {

			if (!deviceArrayAdapter.isEmpty())
				deviceArrayAdapter.clear();

			upnpService.getRegistry().removeAllRemoteDevices();
			upnpService.getControlPoint().search();

			return true;
		} else if (id == R.id.menu_about) {
			AlertDialog.Builder aboutDeviceAlert = new AlertDialog.Builder(
					MainActivity.this);
			aboutDeviceAlert.setTitle(getString(R.string.menu_about_app));
			aboutDeviceAlert.setMessage(getString(R.string.menu_about_text));
			aboutDeviceAlert.show();

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}
	@Override
	public void onPause() {
		super.onPause();
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (upnpService != null)
			upnpService.getRegistry().removeListener(registryListener);
		context.unbindService(upnpServiceConnection);
	}

	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return discoveryFragment;
			case 1:
				return mediaServerFragment;
			case 2:
				return mediaRendererFragment;
			}
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.section_discover).toUpperCase(l);
			case 1:
				return getString(R.string.section_mediaserver).toUpperCase(l);
			case 2:
				return getString(R.string.section_mediarenderer).toUpperCase(l);
			}
			return null;
		}
	}
}
