package pl.umk.mat.protazy.upnpavcontrol;

import java.util.ArrayList;

import org.teleal.cling.support.model.DIDLObject;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MediaServerFragment extends Fragment {

	private FileArrayAdapter fileArrayAdapter;

	public FileArrayAdapter getFileArrayAdapter() {
		return fileArrayAdapter;
	}

	private Context context;

	private ActionHelper getActionHelper() {
		return ((MainActivity) getActivity()).getActionHelper();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		fileArrayAdapter = new FileArrayAdapter(getActivity(),
				R.layout.file_list_item);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_mediaserver,
				container, false);

		context = getActivity();

		ListView fileList = (ListView) rootView.findViewById(R.id.fileListView);

		fileList.setAdapter(fileArrayAdapter);
		fileList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				DIDLObject item = fileArrayAdapter.getItem(position);

				if (fileArrayAdapter.isRootLink(position)) {
					ArrayList<DIDLObject> objectList = getActionHelper()
							.browseAction(getActionHelper().ROOT_DIR);
					fileArrayAdapter.addAll(objectList);
				}

				else if (fileArrayAdapter.isCatalog(position)) {
					ArrayList<DIDLObject> objectList = getActionHelper()
							.browseAction(item.getId());
					fileArrayAdapter.addAll(objectList);
				}

				else if (fileArrayAdapter.isFile(position)) {

					Boolean isUriSet = getActionHelper()
							.setAVTransportUriAction(item);

					if (isUriSet) {
						getActionHelper().playAction();

						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								try {
									Thread.sleep(500);
									getActionHelper().getPositionInfoAction();
								} catch (InterruptedException e) {
								}
							}
						});
					}

				} else

					Toast.makeText(
							context,
							context.getString(R.string.server_avtransport_unavailable),
							Toast.LENGTH_LONG).show();
			}
		});
		return rootView;
	}
}
