package pl.umk.mat.protazy.upnpavcontrol;

import org.teleal.cling.support.model.DIDLObject;
import org.teleal.cling.support.model.item.Item;
import org.teleal.cling.support.model.item.MusicTrack;
import org.teleal.cling.support.model.item.Photo;
import org.teleal.cling.support.model.item.VideoItem;
import org.teleal.cling.support.model.container.Container;
import org.teleal.cling.support.model.container.StorageFolder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FileArrayAdapter extends ArrayAdapter<DIDLObject> {

	public FileArrayAdapter(final Context ctx, final int resource) {
		super(ctx, resource);

		context = ctx;
		inflater = LayoutInflater.from(context);
	}

	private Context context;

	private final LayoutInflater inflater;

	private ViewHolder holder;

	public Boolean isMusicFile(int position) {
		DIDLObject item = getItem(position);
		if (MusicTrack.CLASS.equals(item))
			return true;
		else
			return false;
	}

	public Boolean isImageFile(int position) {
		DIDLObject item = getItem(position);
		if (Photo.CLASS.equals(item))
			return true;
		else
			return false;
	}

	public Boolean isVideoFile(int position) {
		DIDLObject item = getItem(position);
		if (VideoItem.CLASS.equals(item))
			return true;
		else
			return false;
	}

	public Boolean isFile(int position) {
		if (isMusicFile(position) || isImageFile(position)
				|| isVideoFile(position))
			return true;
		else
			return false;
	}

	public Boolean isRootLink(int position) {
		DIDLObject item = getItem(position);
		if (item.getTitle().equals(context.getString(R.string.server_root_dir)))
			return true;
		else
			return false;
	}

	public Boolean isCatalog(int position) {
		DIDLObject item = getItem(position);
		if (StorageFolder.CLASS.equals(item) || item instanceof Container)
			return true;
		else
			return false;
	}

	public String getUri(int position) {
		DIDLObject item = getItem(position);
		return item.getFirstResource().getValue();
	}

	public String getCurrentItemTitle(Item item) {
		String title = context.getString(R.string.renderer_info_unavailable);

		if (item != null && item.getTitle() != null)
			title = item.getTitle();

		return title;
	}

	public String getCurrentItemAlbum(Item item) {
		String album = context.getString(R.string.renderer_info_unavailable);
		if (item != null
				&& item.getFirstPropertyValue(DIDLObject.Property.UPNP.ALBUM.class) != null)
			album = item.getFirstPropertyValue(
					DIDLObject.Property.UPNP.ALBUM.class).toString();

		return album;
	}

	public String getCurrentItemArtist(Item item) {
		String artist = context.getString(R.string.renderer_info_unavailable);
		if (item != null
				&& item.getFirstPropertyValue(DIDLObject.Property.UPNP.ARTIST.class) != null)
			artist = item.getFirstPropertyValue(
					DIDLObject.Property.UPNP.ARTIST.class).toString();

		return artist;
	}

	public String getCurrentItemDuration(Item item) {
		String duration;
		String fixedDuration = context.getString(R.string.renderer_info_unavailable);

		if (item != null && item.getFirstResource() != null
				&& item.getFirstResource().getDuration() != null) {
			duration = item.getFirstResource().getDuration();

			try {
				fixedDuration = duration.substring(0, duration.length() - 4);
			} catch (Exception e) {
				fixedDuration = context.getString(R.string.renderer_info_unavailable);
			}
		}

		return fixedDuration;
	}

	public String getCurrentItemProtocolInfo(Item item) {
		String info = context.getString(R.string.renderer_info_unavailable);
		if (item != null
				&& item.getFirstResource() != null
				&& item.getFirstResource().getProtocolInfo()
						.getContentFormatMimeType() != null)
			info = item.getFirstResource().getProtocolInfo()
					.getContentFormatMimeType().toString();
		return info;
	}

	@Override
	public View getView(final int position, final View convertView,
			final ViewGroup parent) {

		View itemView = convertView;
		holder = null;
		final DIDLObject item = getItem(position);

		if (null == itemView) {
			itemView = this.inflater.inflate(R.layout.file_list_item, parent,
					false);

			holder = new ViewHolder();

			holder.fileName = (TextView) itemView.findViewById(R.id.fileName);
			holder.fileIcon = (ImageView) itemView.findViewById(R.id.fileIcon);

			itemView.setTag(holder);
		} else {
			holder = (ViewHolder) itemView.getTag();
		}

		holder.fileName.setText(item.getTitle());

		try {
			if (isMusicFile(position)) {
				holder.fileIcon.setImageResource(R.drawable.audioxgeneric);
			} else if (isImageFile(position)) {
				holder.fileIcon.setImageResource(R.drawable.imagexgeneric);
			} else if (isVideoFile(position)) {
				holder.fileIcon.setImageResource(R.drawable.videoxgeneric);
			} else if (isCatalog(position)) {
				holder.fileIcon.setImageResource(R.drawable.inodedirectory);
			} else {
				holder.fileIcon.setImageResource(R.drawable.unknown);
			}
		} catch (Exception e) {
			holder.fileIcon.setImageResource(R.drawable.unknown);
		}

		return itemView;
	}

	private static class ViewHolder {

		ViewHolder() {
		}

		private TextView fileName;
		private ImageView fileIcon;
	}
}