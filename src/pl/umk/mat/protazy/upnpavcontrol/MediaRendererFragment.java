package pl.umk.mat.protazy.upnpavcontrol;

import org.teleal.cling.support.model.item.Item;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class MediaRendererFragment extends Fragment {

	private Context context;

	private View rootView;

	private ActionHelper getActionHelper() {
		return ((MainActivity) getActivity()).getActionHelper();
	}

	private TextView titleTextView;
	private TextView authorTextView;
	private TextView albumTextView;
	private TextView mimetypeTextView;
	private TextView durationTextView;

	public void setCurrentItemInfo(Item item) {

		if (getActionHelper() != null && getActionHelper().getFileArrayAdapter() != null) {

			titleTextView.setText(context
					.getString(R.string.renderer_info_title)
					+ getActionHelper().getFileArrayAdapter()
							.getCurrentItemTitle(item));

			albumTextView.setText(context
					.getString(R.string.renderer_info_album)
					+ getActionHelper().getFileArrayAdapter()
							.getCurrentItemAlbum(item));

			authorTextView.setText(context
					.getString(R.string.renderer_info_artist)
					+ getActionHelper().getFileArrayAdapter()
							.getCurrentItemArtist(item));

			durationTextView.setText(context
					.getString(R.string.renderer_info_duration)
					+ getActionHelper().getFileArrayAdapter()
							.getCurrentItemDuration(item));

			mimetypeTextView.setText(context
					.getString(R.string.renderer_info_mimetype)
					+ getActionHelper().getFileArrayAdapter()
							.getCurrentItemProtocolInfo(item));
		} 
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_mediarenderer, container,
				false);

		context = getActivity();

		ImageButton playButton = (ImageButton) rootView
				.findViewById(R.id.playButton);
		ImageButton pauseButton = (ImageButton) rootView
				.findViewById(R.id.pauseButton);
		ImageButton rewButton = (ImageButton) rootView
				.findViewById(R.id.rewindButton);
		ImageButton ffButton = (ImageButton) rootView
				.findViewById(R.id.fastForwardButton);
		ImageButton stopButton = (ImageButton) rootView
				.findViewById(R.id.stopButton);
		ImageButton muteButton = (ImageButton) rootView
				.findViewById(R.id.muteButton);
		ImageButton lowerVolButton = (ImageButton) rootView
				.findViewById(R.id.lowerVolButton);
		ImageButton higherVolButton = (ImageButton) rootView
				.findViewById(R.id.higherVolButton);

		titleTextView = (TextView) rootView.findViewById(R.id.titleTextView);
		authorTextView = (TextView) rootView.findViewById(R.id.authorTextView);
		albumTextView = (TextView) rootView.findViewById(R.id.albumTextView);
		mimetypeTextView = (TextView) rootView
				.findViewById(R.id.mimetypeTextView);
		durationTextView = (TextView) rootView
				.findViewById(R.id.durationTextView);
		
		titleTextView.setSelected(true);
		authorTextView.setSelected(true);
		albumTextView.setSelected(true);
		
		setCurrentItemInfo(null);

		playButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActionHelper().getPositionInfoAction();
				getActionHelper().playAction();
			}
		});

		stopButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActionHelper().getPositionInfoAction();
				getActionHelper().stopAction();
			}
		});

		pauseButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActionHelper().getPositionInfoAction();
				getActionHelper().pauseAction();
			}
		});

		rewButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActionHelper().getPositionInfoAction();
				getActionHelper().seekAction(-15);
			}
		});

		ffButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActionHelper().getPositionInfoAction();
				getActionHelper().seekAction(15);
			}
		});

		muteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActionHelper().getPositionInfoAction();
				getActionHelper().setMuteAction();
			}
		});

		lowerVolButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActionHelper().getPositionInfoAction();
				getActionHelper().setVolumeAction(-1);
			}
		});

		higherVolButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActionHelper().getPositionInfoAction();
				getActionHelper().setVolumeAction(1);
			}
		});

		return rootView;
	}

}
