# README #


### Description ###

UPnPAVControl is an UPnP Audio/Video 1.0 Control Point mobile application, created for Android 4.1 and newer. With this app, you can discover UPnP and DLNA devices on the network, browse multimedia files avaliable on the Media Server, play them on the Media Renderer and control the playback process.


### Additional information ###

This app uses Java/Android UPnP library, called Cling (http://4thline.org/projects/cling), which is available under LGPL 3.0 license.
Project also uses graphics from Oxygen Icons Pack (www.oxygen-icons.org, LGPL 3.0) and Android Action Bar Icon Pack (http://developer.android.com/design/downloads, CC BY 2.5 license). 


### How to install app ###

To install this app, you have to enable 'install from unknown sources' option on your Android device and choose .apk file (if you want to use ADB alternatively, connect your device to the computer and execute command 'adb install [filename_of_the_apk_file])'.


### About this project ###

This app was created by Łukasz Rumiński (lukasz.ruminski 'at' abs.umk.pl).